# Skills Assessment Tool v2.0 - Backend

Skills Assessment Tool (SAT), es una aplicación web que unifica las valoraciones transversales y específicas de los alumnos que realizan un reto.
Un reto (https://observatorio.tec.mx/edutrendsabr) presenta a los alumnos un problema real de su entorno al cual han de buscar una posible solución.
Utilizando SAT obtendremos dos tipos de valoraciones de nuestro alumnado. La primera hace referencia a las habilidades transversales que ha adquirido con el trabajo colaborativo durante el reto. La segunda se obtendrá mediante diversas tareas evaluables específicas en cada reto.
Si deseas utilizar esta herramienta solo necesitarás un servidor web (Apache o Nginx) y una base de datos (MySQL o MariaDB).

