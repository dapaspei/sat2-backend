<?php

namespace App\Controllers;

use App\Models\UserModel;
use App\Models\TeachersModel;
use CodeIgniter\HTTP\Response;
use CodeIgniter\HTTP\ResponseInterface;
use Exception;

class Teachers extends BaseController
{
    /**
     * Get all Teachers
     * @return Response
     */
    public function index()
    {
        $model = new TeachersModel();
        return $this->getResponse(
            [
                'message' => 'Profesores recuperados correctamente',
                'teachers' => $model->findAll()
            ]
        );
    }

    /**
     * Create a new Teacher
     */
    public function store()
    {
      // First Create User
      // $user_rules = [
      //   'username' => 'required|min_length[4]|max_length[50]',
      //   'email' => 'min_length[6]|max_length[50]|valid_email|is_unique[users.email]',
      //   'password' => 'required|min_length[8]|max_length[255]'
      // ];

      // $input = $this->getRequestInput($this->request);

      // if (!$this->validateRequest($input, $rules)) {
      //     return $this->getResponse(
      //             $this->validator->getErrors(),
      //             ResponseInterface::HTTP_BAD_REQUEST
      //         );
      // }

      // $userModel = new UserModel();
      // $userModel->save($input);
      
      // return $this->getJWTForUser(
      //           $input['username'],
      //           ResponseInterface::HTTP_CREATED
      //       );


      // Next Create Teacher
      $rules = [
        'firstname' => 'required',
        'lasttname' => 'required',
        'department' => '',
        't_role' => '',
        'challenge_coordinator' => '',
        'project_coordinator' => '',
        'user_id' => 'required',
        'teaching_levels' => '',
      ];

      $input = $this->getRequestInput($this->request);

        if (!$this->validateRequest($input, $rules)) {
            return $this
                ->getResponse(
                    $this->validator->getErrors(),
                    ResponseInterface::HTTP_BAD_REQUEST
                );
        }

        $teacher_id = $input['user_id'];

        $model = new TeachersModel();
        $model->save($input);
        

        $teacher = $model->where('user_id', $teacher_id)->first();

        return $this->getResponse(
            [
                'message' => 'Profesor añadido correctamente',
                'teacher' => $teacher
            ]
        );
    }

    /**
     * Get a single teacher by ID
     */
    public function show($id)
    {
        try {

            $model = new TeachersModel();
            $teacher = $model->findTeacherById($id);

            return $this->getResponse(
                [
                    'message' => 'Profesor recuperado correctamente',
                    'teacher' => $teacher
                ]
            );

        } catch (Exception $e) {
            return $this->getResponse(
                [
                    'message' => 'No hay ningún profesor con el ID:' + $id
                ],
                ResponseInterface::HTTP_NOT_FOUND
            );
        }
    }

    /**
     * Update a teacher's information
     */
    public function update($id)
    {
        try {

          $model = new TeachersModel();
          $model->findTeacherById($id);

          $input = $this->getRequestInput($this->request);
          //var_dump("<br>update->input:".json_encode($input));
          //die();
          

          $model->update($id, $input);
          $teacher = $model->findTeacherById($id);

          return $this->getResponse(
            [
              'message' => 'Información del profesor correctamente modificada',
              'teacher' => $teacher
          ]
          );

        } catch (Exception $exception) {

          return $this->getResponse(
            [
              'message' => $exception->getMessage()
            ],
            ResponseInterface::HTTP_NOT_FOUND
          );
        }
    }

    /**
     * Delete a teacher
     */
    public function delete($id)
    {
        try {

            $model = new TeachersModel();
            $teacher = $model->findTeacherById($id);
            $model->delete($teacher);

            return $this
                ->getResponse(
                    [
                        'message' => 'Profesor borrado correctamente',
                    ]
                );

        } catch (Exception $exception) {
            return $this->getResponse(
                [
                    'message' => $exception->getMessage()
                ],
                ResponseInterface::HTTP_NOT_FOUND
            );
        }
    }

    // private function getJWTForTeacher(string $id, int $responseCode = ResponseInterface::HTTP_OK)
    // {
    //     try {
    //         $model = new TeachersModel();
    //         $teacher = $model->findTeacherById($id);
    //         var_dump("<br>teacher = ".json_encode($teacher));
    //         die();

    //         helper('jwt');

    //         return $this
    //             ->getResponse(
    //                 [
    //                     'message' => 'Profesor autenticado correctamente',
    //                     'teacher' => $teacher,
    //                     'access_token' => getSignedJWTForUser($teacher['user_id'])
    //                     //'access_token' => getSignedJWTForUser($id)

    //                 ]
    //             );
    //     } catch (Exception $exception) {
    //         return $this
    //             ->getResponse(
    //                 [
    //                     'error' => $exception->getMessage(),
    //                 ],
    //                 $responseCode
    //             );
    //     }
    // }
}
