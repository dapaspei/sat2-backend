<?php

namespace App\Controllers;

use App\Models\ChallengesModel;

use CodeIgniter\HTTP\Response;
use CodeIgniter\HTTP\ResponseInterface;
use Exception;

class Challenges extends BaseController
{
    //helper('App\Helpsers\randomuid');

    /**
     * Get all Challenges
     * @return Response
     */
    public function index()
    {
        $model = new ChallengesModel();
        return $this->getResponse(
        [
          'message' => 'Retos recuperados correctamente',
          'challenges' => $model->findAll()
          //'challenges' => $model->paginate(6),
          //'pager' => $model->pager // NOOO
        ]
        );
    }

    /**
     * Create a new Challenge
     */
    public function create()
    {
      helper("randomuid");
        $rules = [
            'name' => 'required|is_unique[challenges.name]',
            /*'description' => '',
            'year' => '',
            'start_date' => '',
            'finish_date' => '',
            'sprints' => '',
            'sprints_openfor_assess' => '',
            'sprints_date' => '',
            'teams' => '',
            'subject' => '',
            'rubric_id' => '',
            'technical_skills' => '',
            't_owner' => '',
            'teaching_levels' => '',*/
        ];

        $input = $this->getRequestInput($this->request);

        if (!$this->validateRequest($input, $rules)) {
            return $this
                ->getResponse(
                    $this->validator->getErrors(),
                    ResponseInterface::HTTP_BAD_REQUEST
                );
        }

        //$challenge_id = $input['id'];
        $input['teaching_levels'] = json_encode($input['teaching_levels']);

        // Generamos un identificador único para el reto.
        //$uniqueID = gen_uuid();
        //var_dump("Enroll_id = ".json_encode($uniqueID));
        $input['challenge_enroll_id'] = gen_uuid();


        $model = new ChallengesModel();
        $model->save($input);
        

        //$challenge = $model->where('id', $challenge_id)->first();
        $challenge = $model->where('name', $input['name'])->first();


        return $this->getResponse(
            [
                'message' => 'Reto añadido correctamente',
                'challenge' => $challenge
            ]
        );
    }

    /**
     * Get a single challenge by ID
     */
    public function show($id)
    {
        try {

            $model = new ChallengesModel();
            $challenge = $model->findChallengeById($id);

            return $this->getResponse(
                [
                    'message' => 'Reto recuperado correctamente',
                    'challenge' => $challenge
                ]
            );

        } catch (Exception $e) {
            return $this->getResponse(
                [
                    'message' => 'No hay ningún reto con el ID:' + $id
                ],
                ResponseInterface::HTTP_NOT_FOUND
            );
        }
    }

    /**
     * Update a challenge's information
     */
    public function update($id)
    {
        try {

          $model = new ChallengesModel();
          //$model->findChallengeById($id);

          $input = $this->getRequestInput($this->request);
          //var_dump("<br>update->input:".json_encode($input));
          //die();
          //$input['id'] = $id;

          if(!empty($input['teaching_levels']))
            $input['teaching_levels'] = json_encode($input['teaching_levels']);
          

          $model->save($input);
          $challenge = $model->findChallengeById($id);

          return $this->getResponse(
            [
              'message' => 'Reto modificado correctamente.',
              'challenge' => $challenge
          ]
          );

        } catch (Exception $exception) {

          return $this->getResponse(
            [
              'message' => $exception->getMessage()
            ],
            ResponseInterface::HTTP_NOT_FOUND
          );
        }
    }

    /**
     * Delete a challenge
     */
    public function delete($id)
    {
        try {

            $model = new ChallengesModel();
            $challenge = $model->findChallengeById($id);
            //$model->delete($challenge);
            $model->where('id', $id)->delete();

            return $this
                ->getResponse(
                    [
                        'message' => 'Reto borrado correctamente',
                    ]
                );

        } catch (Exception $exception) {
            return $this->getResponse(
                [
                    'message' => $exception->getMessage()
                ],
                ResponseInterface::HTTP_NOT_FOUND
            );
        }
    }

}
