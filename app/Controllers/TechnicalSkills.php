<?php

namespace App\Controllers;

use App\Models\TechnicalSkillsModel;

use CodeIgniter\HTTP\Response;
use CodeIgniter\HTTP\ResponseInterface;
use Exception;

class TechnicalSkills extends BaseController
{
    /**
     * Get all TechnicalSkills
     * @return Response
     */
    public function index()
    {
        $model = new TechnicalSkillsModel();
        return $this->getResponse(
        [
          'message' => 'Retos recuperados correctamente',
          'technical_skills' => $model->findAll()
          //'technical_skills' => $model->paginate(6),
          //'pager' => $model->pager // NOOO
        ]
        );
    }

    /**
     * Create a new technical_skills
     */
    public function create()
    {
      $rules = [
        'name' => 'required|is_unique[technical_skills.name]',
      ];

      $input = $this->getRequestInput($this->request);

      if (!$this->validateRequest($input, $rules)) {
        return $this
          ->getResponse(
            $this->validator->getErrors(),
            ResponseInterface::HTTP_BAD_REQUEST
          );
      }

      // Se hace desde el frontend
      //$input['challenges_in'] = implode(",", $input['challenges_in']);

      $model = new TechnicalSkillsModel();
      $model->save($input);

      //$technical_skills = $model->where('id', $technical_skills_id)->first();
      $technical_skill = $model->where('name', $input['name'])->first();

      return $this->getResponse(
        [
          'message' => 'Reto añadido correctamente',
          'technical_skill' => $technical_skill
        ]
      );
    }

    /**
     * Get a single technical_skill by ID
     */
    public function show($id)
    {
        try {

            $model = new TechnicalSkillsModel();
            $technical_skill = $model->findTechnicalSkillById($id);

            return $this->getResponse(
                [
                    'message' => 'Technical Skill recuperada correctamente',
                    'technical_skill' => $technical_skill
                ]
            );

        } catch (Exception $e) {
            return $this->getResponse(
                [
                    'message' => 'No hay ningún TechnicalSkill con el ID:' + $id
                ],
                ResponseInterface::HTTP_NOT_FOUND
            );
        }
    }

    /**
     * Update a technical_skills's information
     */
    public function update($id)
    {
        try {

          $model = new TechnicalSkillsModel();

          $input = $this->getRequestInput($this->request);
          //var_dump("<br>update->input:".json_encode($input));
          //die();
          //$input['id'] = $id;

          //$input['challenges_in'] = implode(",", $input['challenges_in']);

          $model->save($input);
          $technical_skill = $model->findTechnicalSkillById($id);

          return $this->getResponse(
            [
              'message' => 'Reto modificado correctamente.',
              'technical_skill' => $technical_skill
          ]
          );

        } catch (Exception $exception) {

          return $this->getResponse(
            [
              'message' => $exception->getMessage()
            ],
            ResponseInterface::HTTP_NOT_FOUND
          );
        }
    }

    /**
     * Delete a technical_skill
     */
    public function delete($id)
    {
        try {

            $model = new TechnicalSkillsModel();
            $technical_skill = $model->findTechnicalSkillById($id);

            //$model->delete($technical_skills);
            $model->where('id', $id)->delete();

            return $this
                ->getResponse(
                    [
                        'message' => 'TechnicalSkill borrado correctamente',
                    ]
                );

        } catch (Exception $exception) {
            return $this->getResponse(
                [
                    'message' => $exception->getMessage()
                ],
                ResponseInterface::HTTP_NOT_FOUND
            );
        }
    }

}
