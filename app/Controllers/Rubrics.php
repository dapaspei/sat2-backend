<?php

namespace App\Controllers;

use App\Models\RubricsModel;
use CodeIgniter\HTTP\Response;
use CodeIgniter\HTTP\ResponseInterface;
use Exception;

class Rubrics extends BaseController
{
    /**
     * Get all Rubrics
     * @return Response
     */
    public function index()
    {
        $model = new RubricsModel();
        return $this->getResponse(
            [
                'message' => 'Rúbricas recuperadas correctamente',
                'rubrics' => $model->findAll()
            ]
        );
        // No de puede convertir a Array pq se pasa como texto json
        // Hay que hacerlo en el cliente
        /*
        $model = new RubricsModel();
        $rubrics = $model->findAll();
        // Los datos de la rúbrica los devolvemos como Array
        foreach($rubrics as $rubric) {
          $json_data = $rubric['data'];
          $new_json_data = json_decode($json_data, TRUE);
          $rubric['data'] = $new_json_data;
        }
        return $this->getResponse(
            [
                'message' => 'Rúbricas recuperadas correctamente',
                'rubrics' => $rubrics,
            ]
        );*/
    }

    /**
     * Create a new Rubric
     */
    public function create()
    {
        $rules = [
            'name' => 'required|is_unique[rubrics.name]',
            //'description' => '',
        ];

        $input = $this->getRequestInput($this->request);

        if (!$this->validateRequest($input, $rules)) {
            return $this->getResponse(
                    $this->validator->getErrors(),
                    ResponseInterface::HTTP_BAD_REQUEST
                );
        }

        // Convertimos los Arrays en Strings
        $input['data'] = json_encode($input['data']);
        $input['t_available_for'] = json_encode($input['t_available_for']);
        $input['teaching_levels'] = json_encode($input['teaching_levels']);


        $model = new RubricsModel();
        $model->save($input);
        //$model->insert($input);
        
        $rubric = $model->where('name', $input['name'])->first();

        return $this->getResponse(
            [
                'message' => 'Rúbrica añadida correctamente',
                'rubric' => $rubric
            ]
        );
    }

    /**
     * Get a single rubric by ID
     */
    public function show($id)
    {
        try {

            $model = new RubricsModel();
            $rubric = $model->findRubricById($id);

            return $this->getResponse(
                [
                    'message' => 'Rúbrica recuperada correctamente',
                    'rubric' => $rubric
                ]
            );

        } catch (Exception $e) {
            return $this->getResponse(
                [
                    'message' => 'No hay ninguna rúbrica con el ID:' + $id
                ],
                ResponseInterface::HTTP_NOT_FOUND
            );
        }
    }

    /**
     * Update a rubric's information
     */
    public function update($id)
    {
        try {

          $model = new RubricsModel();
          //$rubric = $model->findRubricById($id);

          $input = $this->getRequestInput($this->request); 
          //$input = $this->request->getPost();

          //$input = $this->request->getRawInput();
          $input['id'] = $id;

          //$json = $this->request->getJSON();
          //var_dump("JSON:".json_encode($json));


          //var_dump("this->request=".json_encode($this->request));
          //var_dump("<br>update->input:".json_encode($input));
          //die();
          
          //$input['t_coordinator'] = $input['t_coordinator'] ? 1 : 0;
          // Convertimos los Arrays en Strings
          if(!empty($input['data']))
            $input['data'] = json_encode($input['data']);
          if(!empty($input['t_available_for']))
            $input['t_available_for'] = json_encode($input['t_available_for']);
          if(!empty($input['teaching_levels']))
            $input['teaching_levels'] = json_encode($input['teaching_levels']);

          //$model->update($id, $input);
          $model->save($input);
          $rubric = $model->findRubricById($id);
          //var_dump("Tras añadir la rúbrica:".json_encode($rubric));

          return $this->getResponse(
            [
              'message' => 'Rúbrica modificada correctamente.',
              'rubric' => $rubric
            ]
          );

        } catch (Exception $exception) {

          return $this->getResponse(
            [
              'message' => $exception->getMessage()
            ],
            ResponseInterface::HTTP_NOT_FOUND
          );
        }
    }

    /**
     * Delete a rubric
     */
    public function delete($id)
    {
        try {
            //var_dump("id a borrar:".$id);
            //die();

            $model = new RubricsModel();
            //$rubric = $model->findRubricById($id);
            //$model->delete($rubric);
            $model->where('id', $id)->delete();

            return $this
                ->getResponse(
                    [
                        'message' => 'Rúbrica borrada correctamente',
                    ]
                );

        } catch (Exception $exception) {
            return $this->getResponse(
                [
                    'message' => $exception->getMessage()
                ],
                ResponseInterface::HTTP_NOT_FOUND
            );
        }
    }

}
