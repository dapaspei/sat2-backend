<?php

namespace App\Controllers;

use App\Models\UsersModel;
use CodeIgniter\HTTP\Response;
use CodeIgniter\HTTP\ResponseInterface;
use Exception;

class Users extends BaseController
{
    /**
     * Get all Users
     * @return Response
     */
    public function index()
    {
        $model = new UsersModel();
        return $this->getResponse(
            [
                'message' => 'Usuarios recuperados correctamente',
                'users' => $model->findAll()
            ]
        );
    }

    /**
     * Create a new User
     */
    //public function store()
    public function create()
    {
        $rules = [
            'username' => 'required|trim|is_unique[users.username]',
            //'firstname' => '',
            //'lastname' => '',
            'email' => 'required|is_unique[users.email]',
            'password' => 'required',
            'role' => 'required',
        ];

        $input = $this->getRequestInput($this->request);

        if (!$this->validateRequest($input, $rules)) {
            return $this->getResponse(
                    $this->validator->getErrors(),
                    ResponseInterface::HTTP_BAD_REQUEST
                );
        }

        // Cambiamos los niveles de aprendizaje por un JSON para almacenarlo en BBDD
        $input['teaching_levels'] = json_encode($input['teaching_levels']);
        $input['t_coordinator'] = $input['t_coordinator'] ? 1 : 0;

        $model = new UsersModel();
        $model->save($input);
        //$model->insert($input);
        
        $user = $model->where('username', $input['username'])->first();

        return $this->getResponse(
            [
                'message' => 'Usuario añadido correctamente',
                'user' => $user
            ]
        );
    }

    /**
     * Get a single user by ID
     */
    public function show($id)
    {
        try {

            $model = new UsersModel();
            $user = $model->findUserById($id);

            return $this->getResponse(
                [
                    'message' => 'Usuario recuperado correctamente',
                    'user' => $user
                ]
            );

        } catch (Exception $e) {
            return $this->getResponse(
                [
                    'message' => 'No hay ningún reto con el ID:' + $id
                ],
                ResponseInterface::HTTP_NOT_FOUND
            );
        }
    }

    /**
     * Update a user's information
     */
    public function update($id)
    {
        try {

          $model = new UsersModel();
          //$model->findUserById($id);

          $input = $this->getRequestInput($this->request); // Funciona con Vue
          //$input = $this->request->getRawInput(); // Funciona con Postman
          $input['id'] = $id;

          //$input = $this->request->getPost();
          //var_dump("<br>update->input:".json_encode($input));
          //die();
          
          // Si la contraseña está vacía no la cambiamos
          //if(!empty($input['password']))
          if(trim($input['password']) === '')
            unset($input['password']);

          // Cambiamos los niveles de aprendizaje por un JSON para almacenarlo en BBDD
          if(!empty($input['teaching_levels']))
            $input['teaching_levels'] = json_encode($input['teaching_levels']);

          //$input['t_coordinator'] = $input['t_coordinator'] ? 1 : 0;

          //$model->update($id, $input);
          $model->save($input);
          //$model->post($id, $input);
          $user = $model->findUserById($id);

          return $this->getResponse(
            [
              'message' => 'Usuario modificado correctamente.',
              'user' => $user
          ]
          );

        } catch (Exception $exception) {

          return $this->getResponse(
            [
              'message' => $exception->getMessage()
            ],
            ResponseInterface::HTTP_NOT_FOUND
          );
        }
    }

    /**
     * Delete a user
     */
    public function delete($id)
    {
        try {
            //var_dump("id a borrar:".$id);
            //die();

            $model = new UsersModel();
            //$user = $model->findUserById($id);
            //$model->delete($user);
            $model->where('id', $id)->delete();

            return $this
                ->getResponse(
                    [
                        'message' => 'Usuario borrado correctamente',
                    ]
                );

        } catch (Exception $exception) {
            return $this->getResponse(
                [
                    'message' => $exception->getMessage()
                ],
                ResponseInterface::HTTP_NOT_FOUND
            );
        }
    }

}
