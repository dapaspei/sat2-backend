<?php

namespace App\Controllers;

use App\Models\UsersModel;
use CodeIgniter\HTTP\Response;
use CodeIgniter\HTTP\ResponseInterface;
use Exception;
use ReflectionException;

class Auth extends BaseController
{
    /**
     * Register a new user
     * @return Response
     * @throws ReflectionException
     */
    public function signup()
    {
      $rules = [
        'username' => 'required|min_length[4]|max_length[50]|is_unique[users.username]',
        'email' => 'min_length[6]|max_length[50]|valid_email|is_unique[users.email]',
        'password' => 'required|min_length[8]|max_length[255]'
      ];

      $input = $this->getRequestInput($this->request);

      if (!$this->validateRequest($input, $rules)) {
          return $this->getResponse(
                  $this->validator->getErrors(),
                  ResponseInterface::HTTP_BAD_REQUEST
              );
      }

      $userModel = new UsersModel();
      $userModel->save($input);
      
      return $this->getJWTForUser(
                $input['username'],
                ResponseInterface::HTTP_CREATED
            );

    }

    /**
     * Authenticate Existing User
     * @return Response
     */
    public function signin()
    {
      $rules = [
        'username' => 'required|min_length[4]|max_length[50]',
        'password' => 'required|min_length[8]|max_length[255]|validateUser[username, password]'
        
      ];

      $errors = [
        //'username' => [
        //    'validateUser' => 'Usuario no existe o incorrecto'
        //],
        'password' => [
          //'validateUser' => 'Contraseña incorrecta'
          'validateUser' => 'Usuario o Contraseña incorrectos'
        ]
      ];

      $input = $this->getRequestInput($this->request);


      if (!$this->validateRequest($input, $rules, $errors)) {
          return $this
              ->getResponse(
                  $this->validator->getErrors(),
                  ResponseInterface::HTTP_BAD_REQUEST
              );
      }
      return $this->getJWTForUser($input['username']);
       
    }

    /**
     * Update an existing user
     * @return Response
     * @throws ReflectionException
     */
    public function update()
    {
      $rules = [
        'username' => 'min_length[4]|max_length[50]|is_unique[users.username]',
        'email' => 'min_length[6]|max_length[50]|valid_email|is_unique[users.email]',
        'password' => 'min_length[8]|max_length[255]',
        'role' => ''
      ];


      $input = $this->getRequestInput($this->request);

      // Quitamos el identificador 'id'
      unset($input);

      if (!$this->validateRequest($input, $rules)) {
          return $this->getResponse(
                  $this->validator->getErrors(),
                  ResponseInterface::HTTP_BAD_REQUEST
              );
      }
      //var_dump("input=".json_encode($input));
      $userModel = new UsersModel();
      $userModel->update($input);
      
      return $this->getJWTForUser(
                $input['username'],
                ResponseInterface::HTTP_CREATED
            );

    }

    /**     
     * Delete an existing user
     * @return Response
     * @throws ReflectionException
     */
    /*public function delete()
    {


      $input = $this->getRequestInput($this->request);

    
      $userModel = new UsersModel();
      $userModel->delete($input);
      
      return $this->getJWTForUser(
                $input['username'],
                ResponseInterface::HTTP_CREATED
            );

    }*/



    private function getJWTForUser(string $username, int $responseCode = ResponseInterface::HTTP_OK)
    {
        try {
            $model = new UsersModel();
            $user = $model->findUserByUserName($username);
            unset($user['password']);

            helper('jwt');

            return $this
                ->getResponse(
                    [
                        'message' => 'Usuario autenticado correctamente',
                        'user' => $user,
                        'access_token' => getSignedJWTForUser($username)
                    ]
                );
        } catch (Exception $exception) {
            return $this
                ->getResponse(
                    [
                        'error' => $exception->getMessage(),
                    ],
                    $responseCode
                );
        }
    }
}
