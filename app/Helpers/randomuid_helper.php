<?php

// Source: https://stackoverflow.com/questions/307486/short-unique-id-in-php
function gen_uuid($len=8): string
{
    $hex = md5("Cuando el grajo vueva bajo ... " . uniqid("", true));

    $pack = pack('H*', $hex);
    $tmp =  base64_encode($pack);

    $uid = preg_replace("#(*UTF8)[^A-Za-z0-9]#", "", $tmp);

    $len = max(4, min(128, $len));

    while (strlen($uid) < $len)
        $uid .= gen_uuid(22);

    return substr($uid, 0, $len);
}