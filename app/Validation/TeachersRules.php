<?php

namespace App\Validation;

use App\Models\TeachersModel;
use Exception;

class TeachersRules
{
    public function validateTeacher(string $str, string $fields, array $data): bool
    {
        try {
            $model = new TeachersModel();
            $teacher = $model->findTeacherById($data['id']);
            // return password_verify($data['password'], $user['password']);
            if (!empty($teacher))
                return true;
            else return false;
        } catch (Exception $e) {
            return false;
        }
    }
}
