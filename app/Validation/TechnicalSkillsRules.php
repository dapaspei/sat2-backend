<?php

namespace App\Validation;

use App\Models\TechnicalSkillsModel;
use Exception;

class TechnicalSkillsRules
{
    public function validateTechnicalSkill(string $str, string $fields, array $data): bool
    {
        try {
            $model = new TechnicalSkillsModel();
            $technicalS_skill = $model->findTechnicalSkillById($data['id']);
            if (!empty($technicalS_skill))
                return true;
            else return false;
        } catch (Exception $e) {
            return false;
        }
    }
}
