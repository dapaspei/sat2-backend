<?php

namespace App\Validation;

use App\Models\ChallengesModel;
use Exception;

class ChallengesRules
{
    public function validateChallenge(string $str, string $fields, array $data): bool
    {
        try {
            $model = new ChallengesModel();
            $challenge = $model->findChallengeById($data['id']);
            // return password_verify($data['password'], $user['password']);
            if (!empty($challenge))
                return true;
            else return false;
        } catch (Exception $e) {
            return false;
        }
    }
}
