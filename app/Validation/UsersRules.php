<?php

namespace App\Validation;

use App\Models\UsersModel;
use Exception;

class UsersRules
{
    public function validateUser(string $str, string $fields, array $data): bool
    {
        try {
            $model = new UsersModel();
            $user = $model->findUserByUserName($data['username']);
            return password_verify($data['password'], $user['password']);
        } catch (Exception $e) {
            return false;
        }
    }
}
