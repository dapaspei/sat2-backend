<?php

namespace App\Models;

use CodeIgniter\Model;
use Exception;

class TechnicalSkillsModel extends Model
{
    protected $table = 'technical_skills';
    protected $primaryKey = 'id';
    protected $allowedFields = [
        'name',
        'description',
        'school_year',
        'has_rubric',
        'rubric_id',
        'challenges_in',
        't_owner',
    ];

    public function findTechnicalSkillByName(string $name)
    {
        $techSkill = $this
            ->asArray()
            ->where(['name' => $name])
            ->first();

        if (!$techSkill) 
            throw new Exception('Reto inexistente');

        return $techSkill;
    }
    public function findTechnicalSkillById($id)
    {
        $techSkill = $this
            ->asArray()
            ->where(['id' => $id])
            ->first();

        // print_r($techSkill);

        if (!$techSkill) throw new Exception('No techSkill with id: '.$id);

        return $techSkill;
    }
}
