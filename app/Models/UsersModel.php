<?php

namespace App\Models;

use CodeIgniter\Model;
use Exception;

class UsersModel extends Model
{
    protected $table = 'users';
    protected $primaryKey = 'id';
    protected $allowedFields = [
      'username',
      'firstname',
      'lastname',
      'email', 
      'password', 
      'password_changed', 
      'role', 
      't_coordinator',
      'status', 
      'avatar',
      'teaching_levels',
    ];
    protected $beforeInsert = ['beforeInsert'];
    protected $beforeUpdate = ['beforeUpdate'];

    protected function beforeInsert(array $data): array
    {
        return $this->getUpdatedDataWithHashedPassword($data);
    }

    protected function beforeUpdate(array $data): array
    {
        return $this->getUpdatedDataWithHashedPassword($data);
    }

    private function getUpdatedDataWithHashedPassword(array $data): array
    {
        if (isset($data['data']['password'])) {
            $plaintextPassword = $data['data']['password'];
            $data['data']['password'] = $this->hashPassword($plaintextPassword);
        }
        return $data;
    }

    private function hashPassword(string $plaintextPassword): string
    {
        //return password_hash($plaintextPassword, PASSWORD_BCRYPT);
        return password_hash($plaintextPassword, PASSWORD_DEFAULT);
    }
                                      
    public function findUserByUserName(string $username)
    {
        $user = $this
            ->asArray()
            ->where(['username' => $username])
            ->first();

        if (!$user) 
            throw new Exception('Usuario no existe');

        return $user;
    }
    public function findUserById(string $id)
    {
        $user = $this
            ->asArray()
            ->where(['id' => $id])
            ->first();

        if (!$user) 
            throw new Exception('Usuario no existe');

        return $user;
    }
}
