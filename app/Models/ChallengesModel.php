<?php

namespace App\Models;

use CodeIgniter\Model;
use Exception;

class ChallengesModel extends Model
{
    protected $table = 'challenges';
    protected $primaryKey = 'id';
    protected $allowedFields = [
        'name',
        'description',
        'year',
        'start_date',
        'finish_date',
        'sprints',
        'sprints_openfor_assess',
        'sprints_date',
        'teams',
        'subject',
        'soft_percentage',
        'hard_percentage',
        'rubric_id',
        'technical_skills',
        't_owner',
        'challenge_enroll_id',
        'teaching_levels',
    ];

    public function findChallengeByName(string $name)
    {
        $challenge = $this
            ->asArray()
            ->where(['name' => $name])
            ->first();

        if (!$challenge) 
            throw new Exception('Reto inexistente');

        return $challenge;
    }
    public function findChallengeById($id)
    {
        $challenge = $this
            ->asArray()
            ->where(['id' => $id])
            ->first();

        // print_r($challenge);

        if (!$challenge) throw new Exception('No challenge with id: '.$id);

        return $challenge;
    }
}
