<?php

namespace App\Models;

use CodeIgniter\Model;
use Exception;

class TeachersModel extends Model
{
    protected $table = 'teachers';
    protected $primaryKey = 'id';
    protected $allowedFields = [
        'firstname',
        'lastname',
        'departament',
        't_role',
        'challenge_coordinator',
        'project_coordinator',
        'user_id',
        'teaching_levels',
    ];

    public function findTeacherById($id)
    {
        $teacher = $this
            ->asArray()
            ->where(['id' => $id])
            ->first();

        // print_r($teacher);

        if (!$teacher) throw new Exception('No hay ningún profesor con el identificador: '.$id);

        return $teacher;
    }
}
