<?php

namespace App\Models;

use CodeIgniter\Model;
use Exception;

class RubricsModel extends Model
{
    protected $table = 'rubrics';
    protected $primaryKey = 'id';
    protected $allowedFields = [
      'name',
      'description',
      'cols',
      'rows',
      'min_value',
      'max_value',
      'rate',
      'data',
      't_owner',
      't_available_for',
      'teaching_levels',
    ];
    //protected $beforeInsert = ['beforeInsert'];
    //protected $beforeUpdate = ['beforeUpdate'];
    protected $beforeInsert = [];
    protected $beforeUpdate = [];

    protected function beforeInsert(array $data): array
    {
      return [];
      //return $this->getCriteriaData($data);
      /** Transformar los items de la rúbrica a JSON */
    }

    protected function beforeUpdate(array $data): array
    {
      return [];
      //return $this->getCriteriaData($data);
    }

    private function getCriteriaData(array $data): array
    {
      return [];
      //if (isset($data['data']['data'])) {
      //  $arrayData = $data['data']['data'];
      //  $data['data']['data'] = json_encode($arrayData);
      //}
      //return $data;
    }

    /*private function hashPassword(string $plaintextPassword): string
    {
        //return password_hash($plaintextPassword, PASSWORD_BCRYPT);
        return password_hash($plaintextPassword, PASSWORD_DEFAULT);
    }*/
                                      
    public function findRubricByName(string $name)
    {
        $rubric = $this
            ->asArray()
            ->where(['name' => $name])
            ->first();

        if (!$rubric) 
            throw new Exception('Rúbrica inexistente');

        return $rubric;
    }
    public function findRubricById(string $id)
    {
        $rubric = $this
            ->asArray()
            ->where(['id' => $id])
            ->first();
        //var_dump("Rúbrica:".json_encode($rubric));

        if (!$rubric) 
            throw new Exception('Rúbrica inexistente');

        return $rubric;
    }
}
