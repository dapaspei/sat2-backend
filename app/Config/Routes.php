<?php

namespace Config;

// Create a new instance of our RouteCollection class.
$routes = Services::routes();

// Load the system's routing file first, so that the app and ENVIRONMENT
// can override as needed.
if (file_exists(SYSTEMPATH . 'Config/Routes.php'))
{
	require SYSTEMPATH . 'Config/Routes.php';
}

/**
 * --------------------------------------------------------------------
 * Router Setup
 * --------------------------------------------------------------------
 */
$routes->setDefaultNamespace('App\Controllers');
$routes->setDefaultController('Home');
//$routes->setDefaultController('Login');
$routes->setDefaultMethod('index');
$routes->setTranslateURIDashes(false);
$routes->set404Override();
$routes->setAutoRoute(true);

/*
 * --------------------------------------------------------------------
 * Route Definitions
 * --------------------------------------------------------------------
 */

// We get a performance increase by specifying the default
// route since we don't have to scan directories.
// $routes->get('/', 'Home::index');

// $routes->match(['get', 'post'], '/', 'Login::index');
// $routes->get('logout', 'Login::logout');
// $routes->match(['get', 'post'], 'profile', 'Users::profile', ['filter' => 'auth']);
// $routes->get('admin/dashboard', 'Admin\Dashboard::index', ['filter' => 'auth']);
// $routes->get('/lang/{locale}', 'Language::index');
// $routes->get('/changeSchoolYear', 'Login::changeSchoolYear', ['filter' => 'auth']);
//$routes->resource('teachers');
//  you can pass in the ‘websafe’ option to have it generate update and delete methods that work with HTML forms
$routes->get('auth',  ['websafe' => 1], ['filter' => 'cors']);
//$routes->resource('users', ['websafe' => 1], ['filter' => 'cors']);
//$routes->resource('rubrics', ['websafe' => 1], ['filter' => 'cors']);
//$routes->resource('teachers', ['websafe' => 1], ['filter' => 'cors']);
//$routes->resource('challenges', ['websafe' => 1], ['filter' => 'cors']);
//$routes->group('teachers', ['namespace' => 'App\Controllers\Teachers'], function($routes) {
//	$routes->resource('teachers');
// }); 

// CHALLENGES
$routes->post('challenges',                'Challenges::create');
$routes->get('challenges',                 'Challenges::index');
$routes->get('challenges/(:segment)',      'Challenges::show/$1');
$routes->put('challenges/(:segment)',      'Challenges::update/$1');
$routes->patch('challenges/(:segment)',    'Challenges::update/$1');
$routes->delete('challenges/(:segment)',   'Challenges::delete/$1');

// USERS
$routes->post('users',                'Users::create');
$routes->get('users',                 'Users::index');
$routes->get('users/(:segment)',      'Users::show/$1');
//$routes->get('users/(:segment)/edit', 'Users::edit/$1');
$routes->put('users/(:segment)',      'Users::update/$1');
$routes->patch('users/(:segment)',    'Users::update/$1');
$routes->delete('users/(:segment)',   'Users::delete/$1');

// RUBRICS
$routes->post('rubrics',                'Rubrics::create');
$routes->get('rubrics',                 'Rubrics::index');
$routes->get('rubrics/(:segment)',      'Rubrics::show/$1');
$routes->put('rubrics/(:segment)',      'Rubrics::update/$1');
$routes->patch('rubrics/(:segment)',    'Rubrics::update/$1');
$routes->delete('rubrics/(:segment)',   'Rubrics::delete/$1');
//$routes->match(['patch', 'post'], 'rubrics', 'Rubrics', ['websafe' => 1], ['filter' => 'auth']);

// TECHNICAL SKILLS
$routes->post('technical-skills',                'TechnicalSkills::create');
$routes->get('technical-skills',                 'TechnicalSkills::index');
$routes->get('technical-skills/(:segment)',      'TechnicalSkills::show/$1');
$routes->put('technical-skills/(:segment)',      'TechnicalSkills::update/$1');
$routes->patch('technical-skills/(:segment)',    'TechnicalSkills::update/$1');
$routes->delete('technical-skills/(:segment)',   'TechnicalSkills::delete/$1');

//$routes->get('{locale}/teachers', 'Teachers\Challenges::index');
// $routes->resource('/{locale}/teachers/challenges');
// $routes->resource('challenges');

/*
 * --------------------------------------------------------------------
 * Additional Routing
 * --------------------------------------------------------------------
 *
 * There will often be times that you need additional routing and you
 * need it to be able to override any defaults in this file. Environment
 * based routes is one such time. require() additional route files here
 * to make that happen.
 *
 * You will have access to the $routes object within that file without
 * needing to reload it.
 */
if (file_exists(APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php'))
{
	require APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php';
}
